#include "types.h"
#include "stm32f0xx_hal.h"

#ifndef _CAN_H_
#define _CAN_H_

#define STANDARD_FORMAT  0
#define EXTENDED_FORMAT  1

#define DATA_FRAME       0
#define REQUEST_FRAME    1

#define DELIMITER				 0b0

#define TAILLE_MAX			 5

#define POLYNOME_CRC 0xc599


#define SetBit(A,k)     ( A[(k/32)] |= (1 << (k%32)) )
#define ClearBit(A,k)   ( A[(k/32)] &= ~(1 << (k%32)) )
#define TestBit(A,k)    ( A[(k/32)] & (1 << (k%32)) )
#define TestBit_guilhem(A,k)    (A[(k/32)]>>(k%32) && (1))

typedef struct  {
    U32	   sof;					// start of frame : 1 bit
    U8     id[11];                 	// 11 ou 29 bit identifier
    U32     rtr;					// rtr : 1 bit
    U32     ctrl_r;				// champ de controle : 2 bits de r�serve
    U32     ctrl_len;           	// Length of data field in bytes (= DLC)
    U8     data[64];            	// Data field
    U32 	   crc;   			    // crc : 15 bits
    U32     crc_delimiter;      	// delimiteur crc 1 bit
    U32	   ack;					// ack 1 bit
    U32     ack_delimiter;      	// delimiteur acquittement 1 bit
    U32	   eof;					// fin de trame : 7 bits recessifs
    U32     intertrame;		    // intertrame : 3 bits recessifs
    U8     format;             	// 0 - STANDARD, 1- EXTENDED IDENTIFIER
	U32    len_data;
} CAN_msg;


	

typedef struct {
	U8 error_crc;
	U8 error_stuff;
	U8 error_form;
	U8 error_ack;
	U8 error_bit;
	U8 error_random;
} CAN_error;



/* Functions defined in module CAN.c */
void CAN_setup         (void);
void CAN_waitReady     (void);

void CAN_wrMsg         (CAN_msg *CAN_TxMsg, U32 *trame_envoi, CAN_error *CAN_err);
void CAN_send		   		 (U32 *trame_envoi,U8 buffer_lenght);
U8   CAN_receive 	   		(U32 *trame_reception);
void CAN_rdMsg					(U32 *trame_reception,CAN_msg *CAN_RxMsg);

void CAN_fillMsg 			 (CAN_msg *CAN_TxMsg, CAN_error *CAN_err);
//void CAN_wrFilter      (U32 id, unsigned char filter_type);
unsigned short calculate_crc(unsigned short input);
short bit_stuffing (U32 *trame, U32 len_trame_full);

extern CAN_msg      CAN_TxMsg;      // CAN messge for sending
extern CAN_msg      CAN_RxMsg;      // CAN message for receiving
extern U32  				CAN_TxRdy;      // CAN HW ready to transmit a message
extern U32  				CAN_RxRdy;      // CAN HW received a message 

extern int GLB_VAR_TIM6_Interrupt;
extern TIM_HandleTypeDef htim6;

#endif // _CAN_H_


