typedef signed char		S8;                     /*!< 8 bits signed, every bit significant. */
typedef short			S16;                    /*!< 16 bits signed, every bit significant. */
typedef long			S32;                    /*!< 32 bits signed, every bit significant. */
typedef long long		S64;                    /*!< 64 bits signed, every bit significant. */

typedef  char	U8;                    /*!< 8 bits unsigned, every bit significant. */
typedef unsigned short	U16;                   /*!< 16 bits unsigned, every bit significant. */
typedef unsigned long	U32;                   /*!< 32 bits unsigned, every bit significant. */
typedef unsigned long	long U64;                   /*!< 32 bits unsigned, every bit significant. */


#define TRUE 1
#define FALSE 0

