#include "CAN.h"
#include "types.h"
#include "string.h"

 
/* differents etats  de la trame CAN */
#define STATE_SOF                   0
#define STATE_ID										1
#define STATE_RTR                   2
#define STATE_CTRL_R                3
#define STATE_CTRL_LEN              4
#define STATE_DATA                  5
#define STATE_CRC                   6
#define STATE_ACK                   7
#define STATE_EOF                   8
#define STATE_INTERTRAME            9
#define STATE_FILL                  10
#define STATE_BIT_STUFFING          11
#define STATE_END_WRITE							12

/* OFFSET Trame */
#define SOF_OFFSET					0
#define ID_OFFSET					1
#define RTR_OFFSET					12
#define CTRL_R_OFFSET				13
#define CTRL_LEN_OFFSET             15
#define DATA_OFFSET                 19
#define CRC_OFFSET                  19
#define CRC_DELIMITER_OFFSET        34
#define ACK_OFFSET                  35
#define ACK_DELIMITER_OFFSET        36
#define EOF_OFFSET                  37
#define INTERTRAME_OFFSET           44
#define ENDTRAME_OFFSET				47

/* 									Data 									*/
static U8 s_state;
int GLB_VAR_TIM6_Interrupt = 0;
TIM_HandleTypeDef htim6;

void CAN_fillMsg (CAN_msg *CAN_TxMsg, CAN_error *CAN_err)
{
		
    strcpy(CAN_TxMsg->id, "11100001001");
		strcpy(CAN_TxMsg->data ,"00010000");
    CAN_err->error_stuff = '0';
    CAN_err->error_ack = '0';
    CAN_err->error_bit = '0';
    CAN_err->error_crc = '0';
    CAN_err->error_form = '0';
    CAN_err->error_random = '0';
    CAN_TxMsg->len_data = 8;
}
 
 void CAN_wrMsg (CAN_msg *CAN_TxMsg, U32 *trame_envoi, CAN_error *CAN_err)
{
	U32 i = 0;
	U32 len_data = CAN_TxMsg->len_data ;
	static U8 len;
	static U32 off;
	
	while(s_state != STATE_END_WRITE)
	{
		switch (s_state)
		{
							
			case STATE_SOF:
				ClearBit(trame_envoi, SOF_OFFSET);
				s_state = STATE_ID;
			break;
			
			case STATE_ID:
				for (i=ID_OFFSET; i<(sizeof(CAN_TxMsg->id) +ID_OFFSET); i++)
				{
					if (CAN_TxMsg->id[i-ID_OFFSET] == '1'){
						
						SetBit(trame_envoi,i);
					} else {
						ClearBit(trame_envoi,i);
					}
				}
				s_state = STATE_RTR;
			break;
			
			case STATE_RTR:
				ClearBit(trame_envoi, RTR_OFFSET);
				s_state = STATE_CTRL_R;
			break;
			
			case STATE_CTRL_R:
				ClearBit(trame_envoi, CTRL_R_OFFSET);
				ClearBit(trame_envoi, (CTRL_R_OFFSET +1));
				s_state = STATE_CTRL_LEN;
			break;
			
			case STATE_CTRL_LEN:
				len = CAN_TxMsg->len_data % 8 ;
				if (len != 0){
					len = (CAN_TxMsg->len_data - len)/8 + 1;
				} else {
					len = CAN_TxMsg->len_data/8;
				}
				for (i=0; i<4; i++)
				{
					off = (CTRL_LEN_OFFSET + 3) - i;
					if (len>>(i) & 1){	
						SetBit(trame_envoi,off);
					} else {
						ClearBit(trame_envoi,off);
					}
				}
				s_state = STATE_DATA;
			break;
			
			case STATE_DATA:
				for (i=DATA_OFFSET; i<(CAN_TxMsg->len_data + DATA_OFFSET); i++)
				{
					if (CAN_TxMsg->data[i-DATA_OFFSET] == '1'){
						SetBit(trame_envoi,i);
					} else {
						ClearBit(trame_envoi,i);
					}
				}
				s_state = STATE_CRC;
			break;
				
			case STATE_CRC:
				ClearBit(trame_envoi, (CRC_OFFSET + len_data));
				SetBit(trame_envoi, (CRC_OFFSET +1+ len_data));
			  SetBit(trame_envoi, (CRC_OFFSET +2+ len_data));
				ClearBit(trame_envoi, (CRC_OFFSET + 3+ len_data));
			  SetBit(trame_envoi, (CRC_OFFSET +4+ len_data));
			  SetBit(trame_envoi, (CRC_OFFSET +5+ len_data));
			  ClearBit(trame_envoi, (CRC_OFFSET + 6+ len_data));
			  SetBit(trame_envoi, (CRC_OFFSET +7+ len_data));
			  ClearBit(trame_envoi, (CRC_OFFSET + 8+ len_data));
			  SetBit(trame_envoi, (CRC_OFFSET +9+ len_data));
			  ClearBit(trame_envoi, (CRC_OFFSET + 10+ len_data));
			  SetBit(trame_envoi, (CRC_OFFSET +11+ len_data));
			  SetBit(trame_envoi, (CRC_OFFSET +12+ len_data));
			  ClearBit(trame_envoi, (CRC_OFFSET + 13+ len_data));
			  SetBit(trame_envoi, (CRC_OFFSET +14+ len_data));
     /*   if (CAN_err->error_crc == '0'){
          trame_envoi[CRC_OFFSET + len_data] = calculate_crc(trame_envoi, (DATA_OFFSET + len_data));
        }
        if (CAN_err->error_form == '0'){
          SetBit(trame_envoi, (CRC_DELIMITER_OFFSET + len_data));
        } else {
          ClearBit(trame_envoi, (CRC_DELIMITER_OFFSET + len_data));
        } */
				s_state = STATE_ACK;
			break;
			
			case STATE_ACK:
				ClearBit(trame_envoi, (ACK_OFFSET + len_data));
				SetBit(trame_envoi, (ACK_DELIMITER_OFFSET + len_data));
				s_state = STATE_EOF;
			break;
			
			case STATE_EOF:
				for (i=0; i<7; i++)
				{
					SetBit(trame_envoi,(EOF_OFFSET + len_data + i));
				}
							
				s_state = STATE_INTERTRAME;
			break;
			
			case STATE_INTERTRAME:
				for (i=0; i<3; i++)
				{
					SetBit(trame_envoi, (INTERTRAME_OFFSET + len_data + i));
				}
				s_state = STATE_BIT_STUFFING;
			break;
			
			case STATE_BIT_STUFFING:
        if (CAN_err->error_stuff == '0')
        {
          bit_stuffing(trame_envoi, (ENDTRAME_OFFSET + len_data));
        }
				s_state = STATE_END_WRITE;
			break;
			
			case STATE_END_WRITE:
			break;
			
			default:
			break;
			
	}
}
	
	
}

unsigned short calculate_crc(unsigned short input) // input only one bit !!!
{
	static unsigned short crc_rg; // shiftregister 
	
	crc_rg = crc_rg << 1;	// leftshift 1 bit
	
	if((input & 1) ^ ((crc_rg>>14)&1))	
	{
		crc_rg = crc_rg ^ 0x4599;
	}
	
	crc_rg = crc_rg & 0x7fff; // only 15 bits

	return crc_rg;
}



short bit_stuffing (U32 *trame, U32 len_trame_full)
{
    volatile U8 count=0, i=0,j=0;
    U32  trame_stuf[5] = {0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF};

    
while(i<len_trame_full) //parcours toute la trame
    {
			if (i==0)
			{
				if (!TestBit(trame, i))
        {
					ClearBit(trame_stuf,j);
        }
        else
        {
          SetBit(trame_stuf,j);
        } 
				i++;
				j++;
      } 
			else
			{
        if(TestBit(trame,i)== TestBit(trame,(i-1))) //true or false
        {
           count++;
           if(count>4) // if 5 times same bit 
             {
               if (!TestBit(trame, i))
                {
                  SetBit(trame_stuf,j);
                  j++;
									ClearBit(trame_stuf,j);
                  count=0;
                 }
               else 
                 {
                  ClearBit(trame_stuf,j);
									j++;
									SetBit(trame_stuf,j);
                  count=0;
                 }                                                            
              }
            else 
             {
                if (!TestBit(trame, i))
								{
									ClearBit(trame_stuf,j);
								}
								else 
								{
  								SetBit(trame_stuf,j);
								}                                            
              }
				}
        else
            {
						  count=0;
							if (!TestBit(trame, i))
              {
                ClearBit(trame_stuf,j);
              }
              else 
              {
                SetBit(trame_stuf,j);
              }                        
            }
        i++;
        j++;
        }
			}
    for (i=0; i<5; i++)
    {
      trame [i]= trame_stuf[i];
    }
    len_trame_full=j;        
    return len_trame_full;
}

void CAN_rdMsg(U32 *trame_reception,CAN_msg *CAN_RxMsg)
 {
	 volatile U32 i = 0, j = 0, cnt = 0;
	 volatile U32 trame_intermediaire[TAILLE_MAX] = {0};
	 volatile U32 endframe_flag = 0;
	 volatile U32 length_frame = 0;
	 // PRINT TRAME RECEPTION
	 
	 // CALCUL LONGUEUR DE TRAME
				do
				{ 
					if(TestBit(trame_reception, i))
						j++;
				  else
						j=0;
					
					if(j==10)
						endframe_flag=1;
					
					i++;
					length_frame = i+1;
				}while((endframe_flag==0)&&(i<160));

	 // DESTUFFING DE TRAME
				
				// trame_reception[0] = trame_intermediaire[0];
				if (TestBit(trame_reception, 0))
        {
					SetBit(trame_intermediaire,0);
        }
        else
        {
          ClearBit(trame_intermediaire,0);
        } 
				
			  cnt = 1;
				j=1;
				for(i=1;i<(length_frame);i++)
			  {
					if(i<(length_frame-13-1))
					{ 
						 //if((trame_reception[i]==trame_reception[i-1]) || (cnt==5))
						 if( (TestBit(trame_reception, i) == ((TestBit(trame_reception, (i-1)))<<0x01) ) || (cnt==5))
						 {
						   if(cnt==5)
							 {
								 i++;
								 
								 //trame_intermediaire[j]=trame_reception[i];
								 if (TestBit(trame_reception, i))
								 {
										SetBit(trame_intermediaire,j);
								 }
								 else
								 {
										ClearBit(trame_intermediaire,j);
								 } 
							   j++;
							   cnt=1;
							 }
							 else
							 {
								 //trame_intermediaire[j]=trame_reception[i];
								 if (TestBit(trame_reception, i))
								 {
										SetBit(trame_intermediaire,j);
								 }
								 else
								 {
										ClearBit(trame_intermediaire,j);
								 } 
							   j++;
								 cnt++;
							 } 
						 }
						 else
						 {
							 	//trame_intermediaire[j]=trame_reception[i];
								 if (TestBit(trame_reception, i))
								 {
										SetBit(trame_intermediaire,j);
								 }
								 else
								 {
										ClearBit(trame_intermediaire,j);
								 }
							  j++;
							  cnt=1;
						 }
					}
					else
					{
						// trame_intermediaire[j]=trame_reception[i];
						if (TestBit(trame_reception, i))
						{
							SetBit(trame_intermediaire,j);
						}
						else
						{
							ClearBit(trame_intermediaire,j);
						} 
						j++;
					}
			  }
		i=0;
		// PRINT TRAME RECEPTION DESTUFFE
				
		// INCLUSION DE LA TRAME DANS UNE STRUCTURE
				
				// SOF
		/*	  CAN_RxMsg->sof = trame_intermediaire[0];
		
				// ID
				for(i=0;i<11;i++)
				{
					CAN_RxMsg->id[i+1] = trame_intermediaire[i+1];
				}
				
				// RTR
				CAN_RxMsg->rtr = trame_intermediaire[12];
					
				// RESERVED BIT	
				for(i=0;i<2;i++)
				{
					CAN_RxMsg->ctrl_r[i] = trame_intermediaire[13+i];
				}
					
				// DLC	
				for(i=0;i<4;i++)
				{
					CAN_RxMsg->ctrl_len[0] = trame_intermediaire[15+i];
				}
				CAN_RxMsg->len_data = (CAN_RxMsg->ctrl_len[0]
													  & CAN_RxMsg->ctrl_len[1]<<0x01
													  & CAN_RxMsg->ctrl_len[2]<<0x02
													  & CAN_RxMsg->ctrl_len[3]<<0x03) * 8;

				// DATA
				if(CAN_RxMsg->len_data != 0) // if DLC!=0
				{
					for(i=0;i<(CAN_RxMsg->len_data*8);i++)
						{
							CAN_RxMsg->data[i] = trame_intermediaire[19+i];
						}
				}
				// CRC
				for(i=0;i<15;i++)
				{
					CAN_RxMsg->crc[i] = trame_intermediaire[19+CAN_RxMsg->len_data+i];
				}

				// CRC DELIMITER
				CAN_RxMsg->crc_delimiter = trame_intermediaire[34+CAN_RxMsg->len_data];
				
				// ACK
				CAN_RxMsg->ack =  trame_intermediaire[35+CAN_RxMsg->len_data];
					
				// ACK DELIMITER
				CAN_RxMsg->ack_delimiter =  trame_intermediaire[36+CAN_RxMsg->len_data];
					
				//END OF FRAME
				for(i=0;i<7;i++)
				{
					CAN_RxMsg->eof[i] =  trame_intermediaire[37+CAN_RxMsg->len_data];
				}
				// INTERFRAME
				for(i=0;i<3;i++)
				{
					CAN_RxMsg->intertrame[i] = trame_intermediaire[44+CAN_RxMsg->len_data+i];
				}
				
				// PRINT STRUCTURE
				*/
 }


void CAN_send(U32 *trame_envoi, U8 buffer_lenght)
{	
	int j = 0;
	//start timer count
	HAL_TIM_Base_Start_IT(&htim6);
	
	//HAL_GPIO_WritePin(GPIOA, GPIO_PIN_1, GPIO_PIN_SET);
	
	do{
		//wait for TIM6 interrupt
		while(GLB_VAR_TIM6_Interrupt == 0);
		GLB_VAR_TIM6_Interrupt = 0;
		
		if(TestBit(trame_envoi,j)) // 1
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_1, GPIO_PIN_SET);
		if(!TestBit(trame_envoi,j)) // 0
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_1, GPIO_PIN_RESET);
		j++;
	}while(j<= buffer_lenght);
	
	// mise � 1 (bit recessif)
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_1, GPIO_PIN_SET);
	
	HAL_TIM_Base_Stop_IT(&htim6);
}

U8   CAN_receive(U32 *trame_reception)
{
	int j =0;
	U8 taille_message = 0;
	GPIO_PinState GPIOA0_Pin_State = GPIO_PIN_RESET;
	
	//wait for message to start incoming
	while(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_0) == GPIO_PIN_SET);	
	HAL_TIM_Base_Start_IT(&htim6);
	
	do{
		// this should read the pin every time interrupt occur
		while(GLB_VAR_TIM6_Interrupt == 0);
		GLB_VAR_TIM6_Interrupt = 0;
		
		GPIOA0_Pin_State = HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_0);
		if(GPIOA0_Pin_State == GPIO_PIN_RESET)
			ClearBit(trame_reception, j);
		if(GPIOA0_Pin_State == GPIO_PIN_SET)
			SetBit(trame_reception, j);
		j++;
	}while(j< 160);
	//stop timer
	HAL_TIM_Base_Stop_IT(&htim6);
	
	return taille_message;
}

